import simple_calc


def test_add():
    assert simple_calc.add(2, 2) == 4
    assert simple_calc.add(-2, 2) == 0
    assert simple_calc.add(2, -2) == 0
    assert simple_calc.add(-2, -2) == -4


def test_subtract():
    assert simple_calc.subtract(2, 2) == 0
    assert simple_calc.subtract(-2, 2) == -4
    assert simple_calc.subtract(2, -2) == 4
    assert simple_calc.subtract(-2, -2) == 0
