import trickey

def test_change_to_15():
	assert trickey.change_to_15(5) == 15
	assert trickey.change_to_15(7) == 15
	assert trickey.change_to_15(-2) == 15
	assert trickey.change_to_15(0) == 15

def test_cube():
	assert trickey.cube(2) == 8
	assert trickey.cube(1) == 1
	assert trickey.cube(-3) == -27
